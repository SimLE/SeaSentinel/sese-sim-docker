FROM px4io/px4-dev-ros-noetic

ENV NVARCH x86_64

ENV NVIDIA_REQUIRE_CUDA "cuda>=11.4 brand=tesla,driver>=418,driver<419 brand=tesla,driver>=450,driver<451"
ENV NV_CUDA_CUDART_VERSION 11.4.108-1
ENV NV_CUDA_COMPAT_PACKAGE cuda-compat-11-4


RUN apt-get update && apt-get install -y --no-install-recommends \
    gnupg2 curl ca-certificates && \
    curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/${NVARCH}/3bf863cc.pub | apt-key add - && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/${NVARCH} /" > /etc/apt/sources.list.d/cuda.list && \
    apt-get purge --autoremove -y curl \
    && rm -rf /var/lib/apt/lists/*

ENV CUDA_VERSION 11.4.2

# For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
RUN apt-get update && apt-get install -y --no-install-recommends \
    cuda-cudart-11-4=${NV_CUDA_CUDART_VERSION} \
    ${NV_CUDA_COMPAT_PACKAGE} \
    && rm -rf /var/lib/apt/lists/*

# Required for nvidia-docker v1
RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf \
    && echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility


RUN apt-get update && apt-get install \
    gcc-arm-none-eabi \
    ros-$ROS_DISTRO-rviz ros-$ROS_DISTRO-plotjuggler-ros \
    ros-$ROS_DISTRO-rqt ros-$ROS_DISTRO-rqt-common-plugins \
    ros-$ROS_DISTRO-navigation \
    # ros-$ROS_DISTRO-behaviortree-cpp-v3 \
    qtbase5-dev libqt5svg5-dev libzmq3-dev libdw-dev libboost-dev \
    # gazebo_dataset_generation
    python3-opencv python3-numpy python3-scipy\
    python3-catkin-tools python3-rosinstall-generator python3-vcstool -y
RUN mkdir -p /workspaces/catkin_ws/src && \
    cd /workspaces/catkin_ws && \
    catkin init

WORKDIR /workspaces/catkin_ws

COPY ./src/deps ./src/deps
RUN rosdep install --from-paths src/deps --ignore-src -y
RUN catkin config  --extend /opt/ros/$ROS_DISTRO && catkin build

COPY ./src ./src
RUN rosdep install --from-paths src src/sese_asv --ignore-src -y
RUN catkin config  --extend /opt/ros/$ROS_DISTRO && catkin build

COPY ./entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]