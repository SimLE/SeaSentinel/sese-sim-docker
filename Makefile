DOCKER_ORG=not7cd
ROS_DISTRO=foxy
D=DOCKER_BUILDKIT=1 docker

build: build-px4-sitl  ## build everything

cmake-install.sh:
	wget https://github.com/Kitware/CMake/releases/download/v3.24.1/cmake-3.24.1-Linux-x86_64.sh -O cmake-install.sh

build-px4-sitl: build-px4-docker  ## build firmware and gazebo plugins
	${D} compose run --rm -e DONT_RUN=1 sitl make px4_sitl gazebo-classic_boat

# .PHONY: build-px4-docker
build-px4-docker: cmake-install.sh  ## build px4-sitl docker image
	mkdir -p .cache/ccache
	${D} build -t ${DOCKER_ORG}/px4-sitl-${ROS_DISTRO} -f ${ROS_DISTRO}.Dockerfile .

shell-sitl:  ## Convinience script to enter shell
	touch .bash_history
	${D} compose run --rm sitl /bin/bash

versions: ## List versions of all programs, both in docker and installed locally
	@echo "Checks versions of all simulation components. Slow!"
	@${D} compose create
	@echo -n "Local ROS1 " && rosversion -d
	@echo -n "Docker ROS1 " && ${D} compose run --rm --no-deps  ros-master bash -c "rosversion -d"
	@echo -n "Local " && gzserver --version | head -n1
	@echo -n "Docker " && ${D} compose run --rm --no-deps  gazebo stdbuf -o L gzserver --version | head -n1
	@echo -n "Local " && mavproxy.py --version | tail -n1
	@echo -n "Docker " && ${D} compose run --rm --no-deps  ardupilot-mavproxy bash -c "mavproxy.py --version" | tail -n1

list-packages: ## List packages installed in sese-sim image
	@echo "Docker ROS packages"
	@${D} compose run --rm --no-deps ros-master bash -c "rospack list-names"


# .PHONY: help
help: ## Display this help.
    @awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

