# Docker Environment for running ASV Simulation

This might be an opinionated docker-compose setup, to run ASV simulation. These components are at play:

* Gazebo 11
* ROS2 Foxy
* PX4-Autopilot


## Dependnecies

* Docker version 20.10.21 or higher
* Nvidia docker runtime
* `make`
* [SimLE / SeaSentinel / roboboat-world · GitLab](https://gitlab.com/SimLE/SeaSentinel/roboboat-world)

roslaunch
- ros
    - ros_gazebo
    - px4_sitl
    - mavros
    - vehicle_spawn
    - asv_controller

deps
- px4 (PX4-Autopilot)
    mavros_posix_launch -> mavlink_sitl_gazebo (world, plugins)
- roboboat_gazebo
    px4.launch -> px4 mavros_posix_sitl.launch + worlds
- asv
    - asv_description
    - asv_gazebo
        launch -> roboboat_gazebo world, asv_description vehicle_spawn

## Getting started

Clone this repo

```bash
mkdir SeSe && cd SeSe
git clone https://github.com/PX4/PX4-Autopilot.git --recursive --shallow-submodules
git clone https://gitlab.com/SimLE/SeaSentinel/sese-sim-docker simulation --recursive
# if submodules changed, try updating them
git submodule update --init --recursive --force
```

Build docker images
```bash
make build
```

Enter bash shell in sitl container
```bash
make shell-sitl
# or if it is running
docker compose exec sitl /entrypoint.sh bash
```

Learn more:
https://docs.px4.io/main/en/test_and_ci/docker.html

## Running

Testing sese_asv
```bash
roslaunch asv_description px4_launch.xml
```

Testing roboboat_gazebo
```bash
# empty world
ros2 launch roboboat_gazebo gazebo_ros_launch.py world:=course_a

# world with PX4 model and SITL
ros2 launch roboboat_gazebo px4_launch.py world:=course_a
```

Get avaiable world with
```bash
ros2 launch roboboat_gazebo gazebo_ros_launch.py --list-args
```
