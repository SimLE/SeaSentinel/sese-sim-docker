ARG ROS_DISTRO=foxy

FROM px4io/px4-dev-ros2-foxy

ENV NVARCH x86_64

ENV NVIDIA_REQUIRE_CUDA "cuda>=11.4 brand=tesla,driver>=418,driver<419 brand=tesla,driver>=450,driver<451"
ENV NV_CUDA_CUDART_VERSION 11.4.108-1
ENV NV_CUDA_COMPAT_PACKAGE cuda-compat-11-4

RUN apt-get update && apt-get install -y --no-install-recommends \
    gnupg2 curl ca-certificates && \
    curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/${NVARCH}/3bf863cc.pub | apt-key add - && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/${NVARCH} /" > /etc/apt/sources.list.d/cuda.list && \
    apt-get purge --autoremove -y curl \
    && rm -rf /var/lib/apt/lists/*

ENV CUDA_VERSION 11.4.2

# For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
RUN apt-get update && apt-get install -y --no-install-recommends \
    cuda-cudart-11-4=${NV_CUDA_CUDART_VERSION} \
    ${NV_CUDA_COMPAT_PACKAGE} \
    && rm -rf /var/lib/apt/lists/*

# Required for nvidia-docker v1
RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf \
    && echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility

# ccache
RUN apt-get update && apt-get install -y --no-install-recommends \
    ccache \
    && rm -rf /var/lib/apt/lists/*
ENV CCACHE_DIR=/ccache
ENV PATH=/usr/lib/ccache:${PATH}

# update CMake, Micro-XRCE-DDS-Agent reqs >= 3.20
# we don't use wget to download this script because it invalidates cache often
COPY cmake-install.sh /tmp/cmake-install.sh
RUN chmod u+x /tmp/cmake-install.sh \
    && mkdir /opt/cmake \
    && /tmp/cmake-install.sh --skip-license --prefix=/opt/cmake \
    && rm /tmp/cmake-install.sh \
    && ln -s /opt/cmake/bin/* /usr/local/bin

# build ~20 min
RUN --mount=type=cache,target=/ccache \
    mkdir -p /workspaces/Micro-XRCE-DDS-Agent && cd /workspaces/Micro-XRCE-DDS-Agent && \
    git clone https://github.com/eProsima/Micro-XRCE-DDS-Agent.git src && \
    mkdir build && cd build && \
    cmake ../src && \
    make -j4 && \
    make install && \
    ldconfig /usr/local/lib/ 

# build 272.9s
RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update && apt-get upgrade -y && apt-get install -y \
    ros-$ROS_DISTRO-ros-base python3-argcomplete \
    ros-$ROS_DISTRO-robot-localization \
    ros-$ROS_DISTRO-teleop-twist-keyboard \
    ros-$ROS_DISTRO-rviz2 \
    ros-$ROS_DISTRO-rviz-common \
    ros-$ROS_DISTRO-rviz-default-plugins \
    ros-$ROS_DISTRO-rviz-visual-tools \
    ros-$ROS_DISTRO-rviz-rendering \
    ros-$ROS_DISTRO-nav2-bringup \
    ros-$ROS_DISTRO-ros2-control ros-$ROS_DISTRO-ros2-controllers \
    ros-$ROS_DISTRO-navigation2 \
    ros-$ROS_DISTRO-vision-msgs \
    ros-$ROS_DISTRO-xacro \
    ros-$ROS_DISTRO-joint-state-publisher \
    ros-$ROS_DISTRO-gazebo-ros-pkgs \
    ros-$ROS_DISTRO-nav2-rviz-plugins \
    python3-catkin-tools \
    python3-opencv \
    python3-pip \
    git-all ccache && \
    apt-get upgrade -y && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN --mount=type=cache,target=/root/.cache/pip \ 
    python3 -m pip install -U \
    pip \
    colcon-common-extensions
RUN --mount=type=cache,target=/root/.cache/pip \
    python3 -m pip install --extra-index-url https://artifacts.luxonis.com/artifactory/luxonis-python-snapshot-local/ \
    depthai

RUN mkdir -p /workspaces/sese_ws/src
WORKDIR /workspaces/sese_ws

# External dependencies with long build time!
# px4_msgs ~10 min
COPY ./src/sese_ws/src/px4_msgs ./src/sese_ws/src/px4_msgs
# COPY ./src/sese_ws/src/px4_ros_com ./src/px4_ros_com
RUN --mount=type=cache,target=/ccache \
    /bin/bash -c "source /opt/ros/$ROS_DISTRO/setup.bash && colcon build"

COPY ./src ./src
RUN --mount=type=cache,target=/ccache \
    /bin/bash -c "source /workspaces/sese_ws/install/setup.bash && colcon build"
# fix workspace folder as build is ran with root
RUN chown -R 1000:1000 ./
COPY ./entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]